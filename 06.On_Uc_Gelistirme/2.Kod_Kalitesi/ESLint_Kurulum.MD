# ESLint Başlangıç Kılavuzu

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/hppJw2REb8g?rel=0" frameborder="0" allowfullscreen></iframe>
*Neden ESLint* @0:00, *ESLint Kurulumu* @2:20.

## Kurulum ve Kullanım

Ön gereklilik: [Node.js](https://nodejs.org/en/) (>=6.14), npm sürüm 3+.

ESLint küresel ve yerli olmak üzere iki biçimde kurulabilir.

### Yerel Kullanım

ESLint'i tasarınızın kurulum dizgesinin bir parçası olmasını istiyorsanız, yerli kurulumu önerilir. Bunu komut satırına şunu yazarak gerçekleştirebilirsiniz:

```
$ npm i eslint --save-dev
```

Sonrasında ise ayar dosyasını kurmalısınız:

```
$ ./node_modules/.bin/eslint --init
```

Bunu da yaptıktan sonra betiğinizi ESLint'e taratabilirsiniz.

```
$ ./node_modules/.bin/eslint betik.js
```

Ayar dosyalarınızı ve eklentilerinizi YEREL olarak eklemeniz gerektiğini unutmayın.

### Küresel Kurulum ve Kullanım

Eğer ESLinti tasarılarınız arasında kullanacaksanı kürsel kurulumu öneririz:

```
$ npm i -g eslint
```

Ayar dosyasını kurun

```
$ eslint --init
```

Artık herhangibir yerde ESLint taraması yapabilirsiniz

```
$ eslint betik.js
```

Ayar dosyalarınızı ve eklentilerinizi KÜRESEL olarak eklemeniz gerektiğini unutmayın.