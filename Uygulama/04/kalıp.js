O.define('Model',{
  DizelgedekiSözcük(sözcük){
    return 'DizelgedekiSözcük'.link('sözlük',sözcük).set(sözcük)
  },
  Sözcük(sözcük){
    return 'Sözcük'.init()
  },
  Dil(dil){
    return 'img'.prop({
      set1(){
        this.set('./img/dil/'+dil+'.svg')
      }
    })
  }
})