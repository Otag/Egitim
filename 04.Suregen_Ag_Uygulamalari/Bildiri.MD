# Web uygulama bildirisi

Web uygulama bildirisi uygulamayla ilgili bilgiler (ad, yazar, açıklama...) veren bir JSON dosyasıdır. 

Web uygulama bildirileri süreğen ağ uygulamaları için belirlenen ölçünlerin bir parçasıdır. 

## Belgeye çağırmak

Web uygulama bildirileri HTML belgesindeki head bölümüne link ögesi eklenerek çağrılır.

```html
<link rel="manifest" href="/bildiri.webmanifest">
```

> Bilgi: .webmanifest uzantısı bildirgedeki Ortam türü kaydı bölümünde bildirilmiştir ama tarayıcılar genellikle diğer uygun uzantıları (.json) da destekler.

## Örnek bildiri

```json
{
 "short_name": "Otağ JS",
 "name": "Otağ JavaScript Çatısı Ağ Yerliği",
 "start_url":"https://otagjs.org/#/belge",
 "display":"standalone",
 "theme_color":"#000",
 "background_color":"#FAFAFA",
 "icons": [
  {
   "src": "img/android-icon-36x36.png",
   "sizes": "36x36",
   "type": "image/png",
   "density": "0.75"
  },
  {
   "src": "img/android-icon-48x48.png",
   "sizes": "48x48",
   "type": "image/png",
   "density": "1.0"
  },
  {
   "src": "img/android-icon-72x72.png",
   "sizes": "72x72",
   "type": "image/png",
   "density": "1.5"
  },
  {
   "src": "img/android-icon-96x96.png",
   "sizes": "96x96",
   "type": "image/png",
   "density": "2.0"
  },
  {
   "src": "img/android-icon-144x144.png",
   "sizes": "144x144",
   "type": "image/png",
   "density": "3.0"
  }
 ]
}
```

## Özellikler

#### background_color

Uygulamanın ardalan rengini belirler. Burada CSS belgesinde belirtilen değer yinelenebilir. Bu belirtim, tarayıcınız uygulamayı başlatırken biçim belgeniz daha hazır değilken bile ardalan rengini çizebilmesini sağlar ve içerik yüklenirken yumuşak bir geçiş oluşturur. 

```json
"background_color": "#F00"
```

> Bilgi: background_color özelliği yalnızca içerik yüklenirken kullanıcı deneyimini artırmak amacıyla düşünülmüştür. Süreğen ağ uygulamanızın biçim(CSS) belgesi yüklendikten sonra background color geçersiz kılınacaktır.


#### description

Ana ekrana eklenen uygulamanızın ne yaptığını anlatan "açıklama" alanıdır.

```json
"description": "Bu uygulama yerleşim alanları arar."
```

#### dir

name, short_name, description alanlarında belirttiğiniz bilgilerin yazım yönünü belirler.
lang (doğal dil) üyesiyle birlikte sağdan sola dillerin doğru görüntülenmesini sağlar.

```json
"dir": "rtl",
"lang": "otk",
"short_name": "𐱅𐰭𐰼𐰃"
```

#### lang

name, short_name, description alanlarında kullandığınız dili belirler

HTML ögesine tanımladığınız **lang** niteliği ile benzerlik gösterir

#### icons

İşletim dizgesi tarafından kullanılacak olan simgeleri bildirmenize yarar. Hangi derinlikte hangi görseli kullanacağınızı belirleyebilirsiniz
```json
"icons": [
  {
    "src": "icon/lowres.webp",
    "sizes": "48x48",
    "type": "image/webp"
  },
  {
    "src": "icon/lowres",
    "sizes": "48x48"
  },
  {
    "src": "icon/hd_hi.ico",
    "sizes": "72x72 96x96 128x128 256x256"
  },
  {
    "src": "icon/hd_hi.svg",
    "sizes": "72x72"
  }
]
```

| Özellik | Açıklama |
|---------|----------|
| src | Görselin yolu |
| type | Dosya türü |
| density | Hedeflenen derinlik (örn: @3x için 3)|

#### name

Uygulama'nın görünür adı
```json
"name": "HacknBreak 2018"
```

#### orientation

Uygulama'nızın görüntülenmesini istediğiniz yön

​​Şu değerleri alabilir:

| Özellik | Açıklama |
|---------|----------|
| any | Belirli bir yön seçilmemiş. |
| natural | Aygıta göre doğal bir yön |
| landscape | yatay kipte görüntüle |
| landscape-primary | birincil olarak yatayı seç |
| landscape-secondary | ikincil olarak yatayı seç |
| portrait | dikey kipte görüntüle |
| portrait-primary | birincil olarak dikeyi seç |
| portrait-secondary | ikincil olarak dikeyi seç |

#### related_applications

Uygulamanın ilişkili olduğu yerli uygulamaların iyeliğindeki bağlantıların dizelgesini içeren bir dizidir.

```json
"related_applications": [
  {
    "platform": "play",
    "url": "https://play.google.com/store/apps/details?id=io.joyin.app",
    "id": "io.joyin.app"
  }, {
    "platform": "itunes",
    "url": "https://itunes.apple.com/app/example-app1/id123456789"
  }]
```


| Özellik | Açıklama |
|---------|----------|
|platform| Yerli uygulamanın dağıtıldığı yer/düzlem |
|url | Uygulamanın indirme bulunağı |
|id | Uygulamanın tanımlayıcı kimliği |

#### prefer_related_applications

Tarayıcı ilişkili uygulamalar alanında bildirilen yerli uygulamaların indirilmesini kullanıcıya seçenek olarak sunar. 

Eğer bunu sunmasını isterseniz true , istemezseniz false olarak bildirinize ekleyebilirsiniz.

#### scope

Uygulamanızın bağlamını bu özellikle bildirirsiniz. KUllanıcı eğer bağlam dışında bir bağlantıya tıklarsa olağan internet yerliği(websitesi) biçiminde açılır. 

Bu, uygulama deneyiminizi etkileyebileceği için üzerinde düşünmeniz gereken bir konudur

```json
"scope": "/uygulama/"
```

#### start_url

Uygulamanızın başlatılacağı URL'dir

```json
"start_url": "/uygulama/dizin.html"
```

#### short_name

Uygulamanızın ana ekranda gözükecek olan kısa adıdır

```json
"short_name": "Hack'nBreak"
```

#### theme_color

Kullanıcı uygulamayı görüntülerken, tarayıcının alacağı rengi belirler. Android'de bulunan Chrome bu özelliğe iyedir.
```json
"theme_color": "black"
```

## Karşılama ekranı

Chrome 47 ve sonrasında uygulama başlatılırken kullanıcıyı karşılayacak bir ekran düşünülmüştür. Bu ekranın içeriği şu biçimdedir.

Belirttiğiniz simgeler içinde 128dpi'a yakın olanı.
_name_ de belirttiğiniz uygulama adı
Ardalanda ise _background_color_ ile bildirdiğiniz ardalan rengi

Kaynak: [MDN](https://developer.mozilla.org/tr/docs/Web/Bildiri)