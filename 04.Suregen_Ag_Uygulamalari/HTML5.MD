# HTML5

![HTML5 İmlek](https://www.completecontrol.co.uk/wp-content/uploads/2017/01/HTML5_Badge_512.png)

HTML5, HTML imleme ölçününün son sürümüdür. Tarayıcılar türlü oranda HTML5 Arabirimlerini desteklemektedir

Tarayıcınızın [HTML5 uyumluluğunu sınayın](https://html5test.com/index.html)

## Yenilikler

HTML sürüm 5 ile birlikte web yertincinde devrim yaratan özellikler eklenmiştir. Bu özellikler uygulamanızın daha fazla kullanılabilir olmasını sağlar ve değerini artırır.

* Gerçek Zamanlı İletişim
* Web Gizimbilim(Cryptology)
* Ortam oynatmasında zaman ayarlaması
* Çevrimdışı Çalışabilme
* [Veri Saklama](./HTML5_Yığınak.MD)
* [Konum](./HTML5_Konum.MD)
* [Titreşim](./HTML5_Titreşim.MD)
* [Ödeme](./HTML5_Ödeme.MD)