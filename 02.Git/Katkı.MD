# Katkı Yapma

1. Bir BENIOKU sayfası oluşturun:
```bash
nano README.MD
```

```markdown
Bu benim GitLab Yığınağım
[Otağ](https://otagjs.org)

```

2. Tasarıda yaptığınız değişiklikleri gözden geçirin:

```bash
git status
```

3. README.MD belgesini ekleyin

```bash
git add README.MD
```

Başka biçimlerde de ekleyebilirsiniz :

```bash
git add .
```

ya da

```bash
git add --all
```

4. Katkı yapın

```bash
git commit
```

Git sizden bir katkı iletisi ister. Yazacağınız ilk satır **başlık**, sonrakiler ise **ayrıntılar** olarak algılanacaktır.

5. Katkı kütüğünü inceleyin

```bash
git log
```

6. Katkınızı uzak yığınağa gönderin

```bash
git push origin master
```
