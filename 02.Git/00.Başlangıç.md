# Git Kurulumu ve yetkilendirme

## Giti bilgisayarınıza kurun

...

## Git ile birlikte kullanmak için bir SSH açarı oluşturun

```bash
ssh-keygen -t rsa -b 4096
```

## SSH açarını SSH ayarlarınıza ekleyin

```bash
nano ~/.ssh/config
```

```
    Host gitlab
        Hostname gitlab.com
        User git
        IdentityFile  ~/.ssh/ACAR
        AddKeysToAgent yes
```
